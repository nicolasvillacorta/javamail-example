# Envio de mails en proyecto Java puro.

## SMTP es un protocolo que se utiliza para realizar el envio de mails.

### La contraseņa y los mails los saque para que no queden pusheados.

Para hacer funcionar esto y poder enviar el mail desde mi Gmail, tuve que ir a los settings de mi cuenta y habilitar el "acceso de aplicaciones poco seguras", sino me tiraba el siguiente error:

Exception in thread "main" javax.mail.AuthenticationFailedException: 535-5.7.8 Username and Password not accepted. Learn more at
535 5.7.8  https://support.google.com/mail/?p=BadCredentials q28sm73055qkn.39 - gsmtp

Para que la conexion pudiera ser segura y evitar esto, no investigue mucho, pero creo que es con certificado, o utilizando OAuth2.


## Es importante  entender que JavaMail no implementa un mail server, es una API que nos da acceso al servidor para enviar desde ahi el correo. Se pueden usar los siguientes 3 protocolos: POP3, IMAP, and SMTP. En el ejemplo yo use el SMTP de Gmail. 

