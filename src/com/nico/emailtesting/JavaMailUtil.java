package com.nico.emailtesting;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class JavaMailUtil {

	public static void sendEmail(String recipient) throws MessagingException {

		System.out.println("Preparing to send email.");

		Properties props = System.getProperties();
		// Tenemos que agregar algunas properties al proyecto que vamos a necesitar para
		// hacer el envio.
		props.put("mail.smtp.host", "smtp.gmail.com"); // Servidor SMTP del proveedor del cual enviaremos el mail.
		props.put("mail.smtp.port", "587"); // El puerto SMTP seguro de Google
		props.put("mail.smtp.auth", "true"); // Usar autenticación mediante usuario y clave
		props.put("mail.smtp.starttls.enable", "true"); // Para conectar de manera segura al servidor SMTP

		String myAccountEmail = "mail_from@gmail.com";
		String myPassword = "password";

		Session session = Session.getInstance(props, new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(myAccountEmail, myPassword);
			}
		});

		Message message = prepareMessage(session, myAccountEmail, recipient);
		Transport.send(message);

		System.out.println("Message sent succesfully");
	}

	private static Message prepareMessage(Session session, String myEmail, String recipient) {

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(myEmail));
			message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
			message.setSubject("my subject");
			message.setContent("Holis", "text/html");
			return message;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}
